package lango

import (
	"bufio"
	"fmt"
	"io/ioutil"
	"math/rand"
	"os"
	"path"
	"strings"
	"time"

	"github.com/agnivade/levenshtein"
)

type Data struct {
	Data      map[string]string
	Questions []string
	// errorsOutput []string
}

type Lango struct {
	training       bool
	learnNew       bool
	score          int
	result         []string
	questionsNum   int
	newRecordsNum  int
	examReportPath string
	recordsPath    string
}

type Option func(s *Lango)

func OptQuestionsNum(i int) Option {
	return func(s *Lango) {
		s.questionsNum = i
	}
}

func OptNewRecordsNum(i int) Option {
	return func(s *Lango) {
		s.newRecordsNum = i
	}
}

func OptExamReportPath(path string) Option {
	return func(s *Lango) {
		s.examReportPath = path
	}
}

func OptRecordsPath(path string) Option {
	return func(s *Lango) {
		s.recordsPath = path
	}
}

func OptTraining(b bool) Option {
	return func(s *Lango) {
		s.training = b
	}
}

func OptLearnNew(b bool) Option {
	return func(s *Lango) {
		s.learnNew = b
	}
}

func NewLango(opts ...Option) *Lango {
	l := &Lango{}
	for _, opt := range opts {
		opt(l)
	}
	return l
}

func (l *Lango) examQuestions() ([]string, *Data, error) {
	var questions []string
	d, err := DataRead(l.recordsPath)
	if err != nil {
		return questions, d, err
	}
	ll := len(d.Questions)
	if ll >= 5 {
		questions = d.Questions[(ll - 5):ll]
		oldQ := d.Questions[0 : ll-5]
		rand.Seed(time.Now().UnixNano())
		rand.Shuffle(len(oldQ), func(i, j int) {
			oldQ[i], oldQ[j] = oldQ[j], oldQ[i]
		})
		if len(oldQ) > 15 {
			oldQ = oldQ[0:15]
		}
		questions = append(questions, oldQ...)
	} else {
		questions = d.Questions
	}
	rand.Shuffle(len(questions), func(i, j int) {
		questions[i], questions[j] = questions[j], questions[i]
	})
	return questions, d, nil
}

func (l *Lango) learnQuestions() ([]string, *Data, error) {
	var questions []string
	d, err := LearnNewRead(l.recordsPath)
	if err != nil {
		return questions, d, err
	}
	questions = d.Questions
	rand.Shuffle(len(questions), func(i, j int) {
		questions[i], questions[j] = questions[j], questions[i]
	})
	return questions, d, nil
}

func (l *Lango) Run() error {
	questions, d, err := l.examQuestions()
	if l.learnNew {
		questions, d, err = l.learnQuestions()
	}
	if err != nil {
		return err
	}
	var res string
	for i, v := range questions {
		res = fmt.Sprintf("%d von %d", i+1, len(questions))
		fmt.Println(res)
		l.result = append(l.result, res)
		err = l.askQuestion(d, v, true)
		if err != nil {
			return err
		}
	}

	res = fmt.Sprintf("Endergebnis: %d Fehler", l.score)
	fmt.Println(res)
	l.result = append(l.result, res)
	l.result = append(l.result, "\n")
	if !l.training {
		return l.save()
	}
	return nil
}

func (l *Lango) save() error {
	t := time.Now()
	name := fmt.Sprintf("%d_%02d_%02d_%dFehler.txt", t.Year(), t.Month(), t.Day(), l.score)
	path := path.Join(l.examReportPath, name)
	text := strings.Join(l.result, "\n")
	err := ioutil.WriteFile(path, []byte(text), 0644)
	if err != nil {
		return err
	}
	return nil
}

func (l *Lango) askQuestion(d *Data, q string, firstRun bool) error {
	var res string

	if firstRun {
		res = fmt.Sprintf("Übersetzen: %s", q)
	} else {
		res = "Versuch es noch einmal"
	}
	fmt.Println(res)
	l.result = append(l.result, res)
	reader := bufio.NewReader(os.Stdin)
	fmt.Print("-> ")
	text, _ := reader.ReadString('\n')
	text = strings.TrimSpace(text)
	res = fmt.Sprintf("-> %s", text)
	l.result = append(l.result, res)
	a, ok := d.Data[q]
	if !ok {
		return fmt.Errorf("unknown record: %s", q)
	}

	e := ErrorValue(text, a)
	switch e {
	case 0:
		res = "Richtig"
		fmt.Println(res)
		l.result = append(l.result, res)
	case -1:
		res = fmt.Sprintf("!! %s", a)
		fmt.Println(res)
		l.result = append(l.result, res)
		res = fmt.Sprintf("Scheitern: -10 Fehler")
		l.result = append(l.result, res)
		l.score -= 10
		if l.training {
			l.askQuestion(d, q, false)
		}
	default:
		score := e * -1
		res = fmt.Sprintf("!! %s", a)
		fmt.Println(res)
		l.result = append(l.result, res)
		res = fmt.Sprintf("Versehen: %d Fehler", score)
		fmt.Println(res)
		l.result = append(l.result, res)
		l.score += score
		if l.training {
			l.askQuestion(d, q, false)
		}
	}
	if firstRun {
		res = fmt.Sprintf("Ergebnis bisher: %d Fehler\n", l.score)
		fmt.Println(res)
		l.result = append(l.result, res)
	}
	return nil
}

func LearnNewRead(path string) (*Data, error) {
	data := make(map[string]string)
	var questions []string
	file, err := os.Open(path)
	if err != nil {
		return nil, err
	}
	defer file.Close()

	scanner := bufio.NewScanner(file)
	new := false
	for scanner.Scan() {
		line := scanner.Text()
		if strings.Contains(line, "---") {
			new = true
		}
		if !new {
			continue
		}
		parts := strings.Split(line, "=")
		if len(parts) != 2 {
			continue
		}
		de := strings.TrimSpace(parts[0])
		eng := strings.TrimSpace(parts[1])
		data[eng] = de
		questions = append(questions, eng)
	}

	if err := scanner.Err(); err != nil {
		return nil, err
	}
	d := &Data{Data: data, Questions: questions}
	return d, nil
}

func DataRead(path string) (*Data, error) {
	data := make(map[string]string)
	var questions []string
	file, err := os.Open(path)
	if err != nil {
		return nil, err
	}
	defer file.Close()

	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		line := scanner.Text()
		if strings.Contains(line, "---") {
			d := &Data{Data: data, Questions: questions}
			return d, nil
		}
		parts := strings.Split(line, "=")
		if len(parts) != 2 {
			continue
		}
		de := strings.TrimSpace(parts[0])
		eng := strings.TrimSpace(parts[1])
		data[eng] = de
		questions = append(questions, eng)
	}

	if err := scanner.Err(); err != nil {
		return nil, err
	}
	d := &Data{Data: data, Questions: questions}
	return d, nil
}

func ErrorValue(candidate string, goldStandard string) int {
	candidate = strings.TrimSpace(candidate)
	if len(candidate) == 0 {
		return -1
	}

	dist := levenshtein.ComputeDistance(candidate, goldStandard)
	if dist > 3 {
		return -1
	}

	return dist
}
