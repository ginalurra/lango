package lango_test

import (
	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"

	. "gitlab.com/ginalurra/lango"
)

var _ = Describe("Lango", func() {
	Describe("DataRead()", func() {
		It("converts file to map", func() {
			d, err := DataRead(vocabularyPath)
			Expect(err).ToNot(HaveOccurred())

			ans, ok := d.Data["good night"]
			Expect(ok).To(Equal(true))
			Expect(ans).To(Equal("gute nacht"))
			ans, ok = d.Data["thank you"]
			Expect(ok).To(Equal(true))
			Expect(ans).To(Equal("danke schön"))
		})
	})

	Describe("ErrorValue()", func() {
		It("returns fail for empty string", func() {
			val := ErrorValue("", "one")
			Expect(val).To(Equal(-1))
		})
		It("returns fail if edit distance > 3", func() {
			val := ErrorValue("kukaracha", "yeah something")
			Expect(val).To(Equal(-1))
		})
		It("returns edit distance, if it is <= 3", func() {
			val := ErrorValue("kukaracha", "fuferacha")
			Expect(val).To(Equal(3))
			val = ErrorValue("danke schon", "danke schön")
			Expect(val).To(Equal(1))
		})
	})
})
