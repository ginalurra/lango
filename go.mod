module gitlab.com/ginalurra/lango

go 1.12

require (
	github.com/agnivade/levenshtein v1.0.2
	github.com/mitchellh/go-homedir v1.1.0
	github.com/onsi/ginkgo v1.12.0
	github.com/onsi/gomega v1.9.0
	github.com/spf13/cobra v0.0.5
	github.com/spf13/viper v1.4.0
)
