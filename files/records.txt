EXAM WORDS

gute nacht = good night
guten abend = good evening
guten tag = good day
bitte = please, you welcome
danke schön = thank you
willkommen zurück = welcome back

-----------------------------
NEW WORDS TO LEARN (use -n flag)

ausschneiden = cut
bearbeiten = edit
kopieren = copy
hinzufügen = add
einfügen = paste
