/*
Copyright © 2019 NAME HERE <EMAIL ADDRESS>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package cmd

import (
	"fmt"
	"log"
	"os"
	"path"

	"github.com/spf13/cobra"

	homedir "github.com/mitchellh/go-homedir"
	"github.com/spf13/viper"
	"gitlab.com/ginalurra/lango"
)

var cfgFile string

type config struct {
	QuestionsNum   int
	NewRecordsNum  int
	ExamReportPath string
	RecordsPath    string
}

// rootCmd represents the base command when called without any subcommands
var rootCmd = &cobra.Command{
	Use:   "lango",
	Short: "A brief description of your application",
	Long: `A longer description that spans multiple lines and likely contains
examples and usage of using your application. For example:

Cobra is a CLI library for Go that empowers applications.
This application is a tool to generate the needed files
to quickly create a Cobra application.`,
	// Uncomment the following line if your bare application
	// has an action associated with it:
	Run: func(cmd *cobra.Command, args []string) {
		versionFlag(cmd)
		opts := getOpts(cmd)
		l := lango.NewLango(opts...)
		err := l.Run()
		if err != nil {
			log.Fatal(err)
		}
	},
}

func getOpts(cmd *cobra.Command) []lango.Option {
	var opts []lango.Option
	cfg := &config{}
	err := viper.Unmarshal(cfg)
	if err != nil {
		log.Fatal(err)
	}

	if cfg.QuestionsNum != 0 {
		opts = append(opts, lango.OptQuestionsNum(cfg.QuestionsNum))
	}
	if cfg.NewRecordsNum != 0 {
		opts = append(opts, lango.OptNewRecordsNum(cfg.NewRecordsNum))
	}
	if cfg.ExamReportPath != "" {
		opts = append(opts, lango.OptExamReportPath(cfg.ExamReportPath))
	}
	if cfg.RecordsPath != "" {
		opts = append(opts, lango.OptRecordsPath(cfg.RecordsPath))
	}

	opts = append(opts, lango.OptTraining(trainingFlag(cmd) || learnNewFlag(cmd)))
	opts = append(opts, lango.OptLearnNew(learnNewFlag(cmd)))
	return opts
}

// Execute adds all child commands to the root command and sets flags appropriately.
// This is called by main.main(). It only needs to happen once to the rootCmd.
func Execute() {
	if err := rootCmd.Execute(); err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
}

func init() {
	cobra.OnInitialize(initConfig)

	// Here you will define your flags and configuration settings.
	// Cobra supports persistent flags, which, if defined here,
	// will be global for your application.

	rootCmd.PersistentFlags().StringVar(&cfgFile, "config", "", "config file (default is $HOME/.config/lango/config.yaml)")

	// Cobra also supports local flags, which will only run
	// when this action is called directly.
	rootCmd.Flags().BoolP("training", "t", false, "Training mode")
	rootCmd.Flags().BoolP("new", "n", false, "learn new words (training mode)")
	rootCmd.Flags().BoolP("version", "v", false, "Version of lango")
}

// initConfig reads in config file and ENV variables if set.
func initConfig() {
	if cfgFile != "" {
		// Use config file from the flag.
		viper.SetConfigFile(cfgFile)
	} else {
		// Find home directory.
		home, err := homedir.Dir()
		if err != nil {
			fmt.Println(err)
			os.Exit(1)
		}

		viper.AddConfigPath(path.Join(home, ".config", "lango"))
		viper.SetConfigName("config")
	}

	viper.AutomaticEnv() // read in environment variables that match

	// If a config file is found, read it in.
	if err := viper.ReadInConfig(); err == nil {
		fmt.Println("Config:", viper.ConfigFileUsed())
	}
}

func trainingFlag(cmd *cobra.Command) bool {
	training, err := cmd.Flags().GetBool("training")
	if err != nil {
		log.Println(err)
		os.Exit(1)
	}
	return training
}

func learnNewFlag(cmd *cobra.Command) bool {
	new, err := cmd.Flags().GetBool("new")
	if err != nil {
		log.Println(err)
		os.Exit(1)
	}
	return new
}

func versionFlag(cmd *cobra.Command) {
	version, err := cmd.Flags().GetBool("version")
	if err != nil {
		log.Println(err)
		os.Exit(1)
	}
	if version {
		fmt.Printf("\nversion: %s\n\ndate:    %s\n\n", lango.Version, lango.Build)
		os.Exit(0)
	}
}
