package lango_test

import (
	"testing"

	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
)

var vocabularyPath = "files/records.txt"

func TestLango(t *testing.T) {
	RegisterFailHandler(Fail)
	RunSpecs(t, "Lango Suite")
}
